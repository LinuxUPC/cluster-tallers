\section{Entorn del terminal i \textit{shells}}

Per a molts pot ser intimidant obrir un terminal, al principi només surt una pantalla negra i no saps exactament que fer. L'entorn minimalista sense cap tipus d'instrucció d'ús fa semblar que sigui una eina feta per a experts. 

És per això que en aquest curs s'ensenyarà el bàsic per a que tu també puguis treure-li tot el partit que puguis a la terminal!

\begin{figure}[ht]
    \centering
    \includegraphics[width = \linewidth]{img/ps1.png}
\end{figure}
Només obrir un terminal el que es veu és una línia de text sobre un fons, normalment, negre. Aquesta línia de text conté per defecte: el nom d'usuari, un \texttt{@}, el nom de la màquina, el directori on s'està treballant actualment i tot seguit un \texttt{\#} si s'ha connectat com a \textit{root} o un \texttt{\$} altrament. Tot això ho veiem abans del cursor on s'escriuen les comandes que es vulguin executar. Aquesta línia es pot editar utilitzant la variable d'entorn \texttt{PS1}, s'explicarà com més endavant.

Per a treballar en una terminal s'ha de conèixer quines comandes accepta, què fan cadascuna d'elles i com s'executen.

Algunes comandes de les més bàsiques són: \texttt{ls} i \texttt{cd}. El que permeten és llistar el contingut d'un directori o canviar al directori indicat, respectivament.
\begin{figure}[ht]
    \centering
    \includegraphics[trim={0 15cm 0 0}, clip, width = \linewidth]{img/ls.png}
\end{figure}

La terminal, o emulador de terminal, és una interfície que executa una \textit{shell}. La shell és bàsicament un programa que llegeix el text d'entrada, el retoca per a que el kernel el pugui entendre per a executar-lo i escriu el resultat. Existeixen vàries shells, però la majoria de característiques són comunes.
\texttt{Bash} és la shell que hi ha per defecte en la majoria de sistemes i la més coneguda.

La majoria de comandes accepten opcions i/o arguments. Les opcions poden començar amb un \texttt{-}, i es posen a continuació de la comanda que es vol executar. Aquestes opcions i arguments serveixen per modificar parcialment el funcionament de la comanda.

Un exemple d'opció d'una comanda és \texttt{ls -l}. Aquesta comanda fa un llistat dels elements que hi ha al directori, però donant molta més informació sobre els fitxers. Una altra opció és \texttt{ls -a}, la qual mostra també els elements ocults (aquells que el seu nom comença per un punt). Les opcions es podem combinar si són compatibles, és a dir, es poden fer servir alhora concatenant-les, com per exemple: \texttt{ls -la}.
\begin{figure}[ht]
    \centering
    \includegraphics[trim={0 5cm 0 0}, clip, width = \linewidth]{img/ll.png}
\end{figure}

Un altre exemple, però d'argument, és posar una direcció (o \textit{path}) a \texttt{ls}, així llista els fitxers d'aquell directori en comptes del directori on estàs situat. La comanda \texttt{pwd} (\textit{print working directory}, en anglès) permet veure a quin directori s'està treballant actualment.

Hi ha 2 maneres d'escriure els \textit{paths}: absoluts o relatius. Els \textit{paths} absoluts són aquells que s'escriuen de forma completa, és a dir, desde el directori arrel (el \texttt{/}) fins al punt que vols. Els \textit{paths} relatius són aquells que s'escriuen de forma relativa al directori on ets ara mateix. Es pot accedir al directori superior utilitzant el directori especial \texttt{..}, que és present a tots els directoris.
\begin{figure}[ht]
    \centering
    \includegraphics[trim={0 15cm 0 0}, clip, width = \linewidth]{img/paths.png}
\end{figure}

\subsection{Mecanismes d'ajuda per a l'usuari}
És impossible saber-se totes les comandes, hi ha moltissimes!! A més a més, saber-se totes les opcions que accepten? Impossible!
Hi ha molts mecanismes que ajuden a no haver de memoritzar tot el que anem descobrint.

\subsubsection{El manual}
El primer és el manual de Linux! El manual de Linux proporciona informació \textbf{detallada} per a qualsevol programa (quasi tots) que estigui instal·lat en el sistema. El seu contingut està separat en diverses seccions. La primera conté informació sobre que fa cada programa instal·lat o comanda de la shell i \textbf{totes} les opcions i arguments que accepta cadascun d'ells, què fan i com s'utilitzen, els codis de retorn, etc. Bàsicament, tot el que necessitis saber en tot moment.
La segona i tercera secció contenen informació sobre les crides a sistema del \textit{kernel} Linux i de les seves llibreries, especialment útil quan es programa en C.
Existeixen altres seccions, entre elles, una que conté informació sobre fitxers especials com pot ser \texttt{/etc/passwd}.

Per accedir a la pàgina del manual d'alguna aplicació simplement s'ha d'executar la comanda \texttt{man} seguida de la pàgina que es vulgui consultar. Per exemple, es pot obrir la pàgina del manual de la pròpia comanda \texttt{man} executant:
\begin{boxed}
      \texttt{\$ man man}
\end{boxed}

\begin{figure}[ht]
    \centering
    \includegraphics[width = \linewidth]{img/manman.png}
\end{figure}

Les pàgines del manual tenen un funcionament molt bàsic. Es poden utilitzar les fletxes per a fer \textit{scroll} cap avall i cap amunt (les tecles \texttt{j} i \texttt{k} fan el mateix, respectivament) i es pot sortir pitjant la tecla \texttt{q}.\\
Hi ha situacions on 2 seccions contenen una pàgina amb el mateix nom. Un d'aquests casos és \texttt{read}. El de la secció 1 correspon a una comanda de la shell i el de la secció 2 a la crida a sistema. En aquests casos, per poder veure la pàgina del manual de la crida a sistema se li haurà d'especificar a la comanda \texttt{man} quina secció ha d'obrir. Això s'aconsegueix de la següent manera:
\begin{boxed}
      \texttt{\$ man 2 read}
\end{boxed}
Hi ha situacions on no vols veure la pàgina sencera, simplement vols veure una petita descripció per decidir si aquesta és l'aplicació que busques. En aquests casos es pot utilitzar la comanda \texttt{whatis}, que dóna una breu descripció de normalment una línia.

L'última comanda relacionada amb el manual és \texttt{apropos}.
Cada pàgina del manual té una petita descripció, aquesta és la que mostra la comanda \texttt{whatis}. La comanda \texttt{apropos} el que fa és buscar noms de pàgines i paraules en aquestes descripcions que coincideixin amb les indicades a buscar. Per exemple, per veure aplicacions que tinguin a veure amb la xarxa, es pot executar: 
\begin{boxed}
      \texttt{\$ \texttt{apropos network}}
\end{boxed}

S'ha de tenir present que \texttt{apropos} utilitza una base de dades per fer aquesta cerca més ràpida i a mesura que s'afegeixen més pàgines s'ha de actualitzar aquesta base de dades, mitjançant:
\begin{boxed}
      \texttt{\$ \texttt{mandb}}
\end{boxed}

\subsubsection{Prémer el tabulador}
El segon mecanisme és completar la comanda prement el tabulador. Al pitjar \texttt{tab}, la terminal consulta el text que hi ha escrit (tot i que estigui a mitges) i intenta acabar d'escriure l'última paraula amb una opció vàlida. Si només existeix una opció possible, completa la paraula amb aquesta. Si n'hi ha més d'una no completarà el text, però et dóna l'opció de prémer el \texttt{tabulador} un segon cop per a mostrar-te totes les opcions que ha trobat per a completar.

Pot completar 2 tipus de paraules: comandes que estiguin al sistema, les quals troba utilitzant la variable \texttt{PATH} de la que es parlarà més endavant, o pot completar arxius que estiguin al directori i seguir direccions a través de directoris.


\subsubsection{Historial}
La shell manté un historial de totes les comandes que es van executant per a cada usuari. Aquest historial està guardat en un fitxer dins del \texttt{home} de cada usuari.
Hi ha diverses accions que ens deixen aprofitar-nos d'aquest historial per no haver de reescriure comandes molt llargues o per si no recordem com s'escrivia exactament. 

La pròpia comanda \texttt{history} mostra a la terminal les últimes comandes que s'han escrit. Aquesta comanda és molt útil, però simplement amb les fletxes d'amunt i avall es pot navegar per l'historial sencer. A més a més, et deixa la línia preparada per prémer \texttt{enter} i executar-la, sense haver de reescriure-la. 

Una altra forma d'aprofitar aquest historial és amb el que s'anomena \textit{reverse search}. A aquesta funció s'accedeix amb la combinació de tecles \texttt{ctrl + r}, a continuació comences a escriure una comanda i et va responent amb la comanda del historial amb la que més coincideix el text inserit. Es va navegant per les coincidencies mitjançant la mateixa combinació de tecles, \texttt{ctrl + r}, i quan t'ensenya la que vols la pots executar només has d'apretar \texttt{enter}.

\subsubsection{Altres Comandes}
Hi ha moltes més comandes que ajuden a l'usuari a situar-se dins de l'entorn de Linux. Algunes d'aquestes són \texttt{which} i \texttt{locate}.

\begin{boxed}
    \textbf{Exercici 1.} Utilitzant el manual, sabríeu dir què fan aquestes dues comandes i com s'utilitzen?
    Què necessita \texttt{locate} per a que pugui funcionar? (Nota: els usuaris d'Arch Linux tenen \texttt{locate} al paquet \texttt{mlocate})
\end{boxed}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Treballant amb arxius i directoris}
Per a tractar amb arxius s'ha de saber quins tipus d'arxius es tenen. Quan es llisten els arxius d'algun directori amb \texttt{ls} es poden tenir dificultats a l'hora d'identificar quins tipus d'arxius es tenen, ja que Linux no treballa amb extensions en el nom, és a dir, els \texttt{.pdf} o \texttt{.txt} al final del nom, entre altres. Es poden posar, però no són necessaris ja que el propi sistema operatiu ja sap identificar què és cada arxiu. 

Per a aquests casos existeix la comanda \texttt{file} que determina el tipus dels documents que li indiques, tinguin extensió o no.
\begin{figure}[ht]
    \centering
    \includegraphics[trim={0 0cm 0 0}, clip, width = \linewidth]{img/file.png}
\end{figure}

El primer de tot és saber com crear arxius i directoris. Amb \texttt{mkdir} es poden crear un o més directoris:
\begin{boxed}
\texttt{\$ mkdir directori}
\end{boxed}

Hi ha varies formes de crear arxius, ja sigui amb editors de text, redireccionant la sortida d'una comanda (es veura com més endavant), etc.
Si simplement es vol crear un arxiu es pot fer amb la comanda \texttt{touch}, que simplement el que fa és actualitzar la data de modificació dels arxius que li indiques, però si algun dels arxius no existeix en crea un de buit amb aquell nom.

També és necessari saber com moure, copiar, canviar noms i esborrar documents. Això s'aconsegueix amb les comandes:
\begin{boxed}
\texttt{\$ cp}\\
\texttt{\$ mv}\\
\texttt{\$ rm}
\end{boxed}

La primera, \texttt{cp}, serveix per crear còpies de documents. S'indica quin (o quins) arxiu vols copiar i una direcció de destí i crearà aquest nou document. Si s'està intentant copiar un conjunt de documents, per exemple tots els pdfs d'un directori, es pot fer servir el caràcter especial ``*''. Aquest caràcter significa ``qualsevol conjunt de caracters''.\\

Si tenim 3 arxius de text (\texttt{arxiu1.txt}, \texttt{arxiu2.txt} i \texttt{arxiu3.txt}) en un directori i els volem copiar al directori \texttt{backup} que està en el mateix directori, les 3 següents línies aconsegueixen el mateix:
\begin{boxed}
\texttt{\$ cp arxiu1.txt arxiu2.txt arxiu3.txt backup}\\
\texttt{\$ cp *.txt backup}\\
\texttt{\$ cp arxiu* backup}
\end{boxed}

En canvi, si només volem duplicar un sol document, a aquest se li pot posar un nou nom afegint-lo a la direcció de destí:
\begin{boxed}
\texttt{\$ cp arxiu1.txt backup/nou\_nom.txt}
\end{boxed}

La segona comanda, \texttt{mv}, funciona de forma semblant, però en comptes de duplicar els documents els mou, és a dir, no et quedes amb l'arxiu original. Aquesta comanda serveix per moure documents o per canviar el nom de documents, això s'aconsegueix movent l'arxiu al mateix directori on està.

Per a esborrar arxius es fa servir la comanda \texttt{rm}. El mateix de sempre, hi indiques un document o un conjunt de documents i els esborra. Però atenció! S'ha d'anar amb compte amb el que s'elimina, ja que aquesta comanda no té una paperera de reciclatge i tot el que s'esborra queda esborrat per sempre. Una opció interessant és l'opció \texttt{-r}. Aquesta opció permet esborrar tots els elements d'un directori de forma recursiva, així no s'han d'eliminar tots els fitxers de dins d'un directori un per un per a poder-lo esborrar.

El pròxim pas és aprendre a llegir o saber com mostrar el contingut d'aquests documents. A part dels editors de text existeixen eines més simples si el que es busca és únicament mostrar el text que conté.

La primera comanda és \texttt{cat}, que imprimeix a la terminal el contingut dels documents que li indiques. Si li dius únicament un document mostrarà aquest document, però si li indiques més d'un els concatenarà, d'aquí el nom, a l'hora de mostrar-los. Hi ha altres comandes útils que també treuen el contingut de fitxers per la terminal, com són \texttt{head} i \texttt{tail}, que únicament mostren les 10 primeres o les 10 últimes línies dels documents, respectivament. El nombre de línies es pot canviar afegint l'opció \texttt{-n}.

En canvi, \texttt{less} mostra el contingut del document en una ``pantalla apart'' on es pot fer \textit{scroll} amb les fletxes per passar de pàgines.
Aquesta comanda és molt més útil per a documents més llargs i al sortir, prement la tecla \texttt{q}, no deixa tot el document a la pantalla, per això l'expressió de la ``pantalla apart''. A més a més, té funcionalitats extra, com poder buscar paraules o saltar directament a la línia que es vulgui. Tota la informació està, com ja sabeu, al manual! 
Que per cert, si us heu fixat, el manual utilitza \texttt{less} per mostrar les seves pàgines.

Si hi ha arxius executables, aquests es poden executar escrivint el \textit{path} fins a l'arxiu. El problema ve quan l'arxiu es troba a la mateixa carpeta on estem, ja que el \textit{path} és simplement el nom de l'arxiu i la nostra \textit{shell} intentarà executar una comanda que es digui com el nostre arxiu en comptes d'executar l'arxiu. Per evitar això s'utilitza el directori especial \texttt{.}, que està present en tots els directoris i representa el mateix directori on som ara mateix. Així podem escriure \texttt{./script.sh} per executar l'arxiu i evitar que la \textit{shell} es confongui.

També existeixen editors de text per a la terminal com \texttt{vi} o \texttt{nano}. L'editor \texttt{nano} és un editor molt simple i mostra totes les seves característiques a la pantalla. L'editor \texttt{vi} és un editor \textbf{molt} potent, tant que hi ha molta gent que l'utilitza com el seu editor únic i per defecte. A més, és important aprendre les comandes bàsiques d'aquest editor, ja que aquest editor està present en tots els sistemes.


\subsection{Execució de comandes}
A l'hora d'executar comandes, hi ha varies formes d'executar-les en cadena, canviant el seu comportament segons el resultat de la comanda anterior.

La primera, la més bàsica, és executar dues comandes amb un \texttt{;} entre elles.
La \textit{shell} executarà les comandes com si escriguéssim primer la primera, l'executéssim, i després escriguéssim i executéssim la segona.
\begin{boxed}
\texttt{\$ comanda 1; comanda2}
\end{boxed}

També es poden lligar comandes de forma condicional. Amb un \texttt{\&\&} la segona comanda s'executarà si la primera ha acabat la seva execució sense donar cap error, és a dir, retornant un \texttt{0} o \texttt{true}.
Amb \texttt{||} s'aconsegueix el contrari: s'executarà la segona si la primera no s'ha pogut executar.
\begin{figure}[ht]
    \centering
    \includegraphics[trim={0 0cm 0 0}, clip, width = \linewidth]{img/or.png}
\end{figure}

A part de lligar comandes es pot tractar el seu output de diverses formes. Amb \texttt{> arxiu} es pot escriure l'output d'una comanda a l'arxiu que es vulgui. Per exemple, \texttt{echo "HOLA" $>$ output} crearia un arxiu \texttt{output} que tindria únicament ``HOLA'' a dins. La comanda \texttt{echo} simplement escriu a la terminal el mateix que escriguis a continuació.
Amb \texttt{2> arxiu} s'aconsegueix el mateix però amb la sortida d'error, en comptes de l'output. I amb \texttt{\&> arxiu} s'aconsegueix enviar el contingut dels 2 canals (output i error) al mateix arxiu. En qualsevol de les opcions anteriors es pot utilitzar \texttt{>>} en comptes de \texttt{>} i el que farà es escriurw aquest output al final del document indicat, en comptes de sobreescriure tot el que hi hagi en aquell arxiu.

Es pot aconseguir lligar comandes, però també lligar els seus outputs amb un sol mitjançant \textit{pipes}, les quals s'indiquen a la terminal amb \texttt{|}. Així s'aconsegueix que la segona comanda llegeixi com a entrada l'output de la primera.

Aquesta funcionalitat és molt potent si se sap utilitzar. Un exemple seria passant-li la sortida d'una comanda com a entrada a la comanda \texttt{grep}. Aquesta comanda serveix per filtrar text: indiques un text i un string i escriurà a la pantalla únicament les línies que continguin aquest string. Es pot utilitzar sobre un arxiu: \\
\begin{boxed}
\texttt{\$ grep string arxiu}
\end{boxed}

O pot filtrar el que rep com a entrada:\\
\begin{boxed}
\texttt{\$ comanda | grep string}
\end{boxed}



Moltes de les comandes de Linux funcionen d'aquesta manera, com \texttt{cat}, \texttt{less}, etc. Això es pot aprofitar de moltes maneres diferents.

\subsection{Superusuari}
Hi ha certs moments on es necessita executar certes comandes com a superusuari, l'usuari \textit{root}, ja que o bé algunes aplicacions ho requereixen o bé les aplicacions, així, poden accedir a fitxers més elevats. També és necessari quan es volen modificar alguns fitxers de fora del teu directori \texttt{home}, com per exemple si es vol canviar la configuració a nivell de sistema.
Això es podria aconseguir iniciant sessió com a \textit{root}, però no és gens recomanable perquè es tenen permisos total per a modificar la configuració del sistema i es una font de possibles problemes de seguretat.

Es poden executar comandes com a superusuari d'altres formes. Una d'elles és \texttt{su}, que permet enganyar al sistema i canviar el teu id d'usuari per el d'un altre, per exemple \textit{root}. Es pot utilitzar de varies maneres, però les dues més bàsiques són:
\begin{boxed}
\texttt{\$ su -c <comanda en mode superusuari>}
\end{boxed}

O si es vol executar un seguit de comandes:
\begin{boxed}
\texttt{\$ su}\\
\texttt{\# comanda 1}\\
\texttt{\# comanda 2}\\
\texttt{\# exit}
\end{boxed}

Hi ha una altre manera d'executar comandes com a superusuari, és molt més coneguda i utilitzada: \texttt{sudo}. Aquesta comanda és molt més extensa, però, bàsicament és escriure \texttt{sudo} seguit de la comanda que vulguis i aquesta s'executarà com a superusuari.
A més a més, \texttt{sudo} té un arxiu de configuració que permet definir quins usuaris o grup tenen accés a \texttt{sudo} o també decidir quines comandes es poden executar a través de \texttt{sudo} i quines no. Aquest arxius es troba a \texttt{/etc/sudoers}.

S'ha de tenir present que fer comandes com a superusuari pot ser molt perillós, amb un simple \texttt{rm} es poden borrar arxius importants del sistema que provoqui que no es pugui encendre més. Així que aneu amb compte :).

\subsection{Modificar l'entorn}
Existeix una comanda molt interessant anomenada \texttt{alias}. Aquesta comanda permet substituir una comanda o un seguit de comandes, amb o sense opcions addicionals, per una cadena més simple, tal com es mostra:
\begin{boxed}
\texttt{\$ alias nomCurt="qualsevol comanda que es vulgui abreviar"}
\end{boxed}

Per exemple:
\begin{boxed}
\texttt{\$ alias conf="cd $\sim$/.config; ls -a"}
\end{boxed}

A partir d'ara, en aquesta \textit{shell}, quan s'executi \texttt{conf} executaran les 2 comandes que li hem assignat.
Es pot desfer aquest alias executant \texttt{unalias conf}. També es pot mostrar el contingut d'aquest alias executant \texttt{alias conf}, o tots els alias del sistema executant simplement \texttt{alias}.

A part dels alias, les variables d'entorn també poden canviar el comportament de la \textit{shell}. Es poden veure aquestes variables simplement executant la comanda \texttt{env}. Existeixen moltes variables d'entorn i s'expliquen totes en la pàgina del manual de la \textit{shell}.

Algunes de les variables més utilitzades són:
\begin{itemize}
    \item \texttt{HOME}: Indica el directori \textit{home} de l'usuari actual. És el directori per el que es substitueix el character $\sim$. \texttt{cd} sense cap argument torna a aquest directori.
    \item \texttt{PATH}: És una llista de directoris separada per un ``:''. La \textit{shell} utilitza l'ordre d'aquesta llista per buscar les comandes que se li demanen. D'aquí es pot deduïr que les comandes no són més que programes instal·lats en el nostre sistema.
    \item \texttt{LANG o LANGUAGE}: Indica l'idioma local, que utilitzaran els programes si es possible.
    \item \texttt{PWD}: Guarda el directori actual. Aquesta variable canvia cada cop que fem \texttt{cd} a un nou directori, per exemple.
\end{itemize}

Per saber el valor d'una variable simplement fa falta imprimir-la:
\begin{boxed}
\texttt{\$ echo \$variable}
\end{boxed}

La comanda \texttt{echo} escriu a la consola el mateix que li poses a continuació. La \textit{shell} es limita a substituir les variables (indicades amb un \$ davant del seu nom) per el seu contingut abans de fer l'execució. Per exemple \texttt{echo \$PWD} imprimeix a la pantalla el directori actual (que és, bàsicament, el que fa la comanda \texttt{pwd}).

Crear o modificar variables també és relativament fàcil: \texttt{variable=valor}. Però s'ha de tenir en compte que cada \textit{shell} és independent a les altres i la creació o modificació de variables es queda a la \textit{shell} on s'ha executat. Les comandes \texttt{export} i \texttt{source} ajuden a contrarrestar aquest fet.

La primera fa que les \textit{shells} filles heredin les variables (quan s'executa un script realment s'executa en una \textit{shell} filla).

\begin{figure}[ht]
    \centering
    \includegraphics[trim={0 2cm 0 0}, clip, width = \linewidth]{img/export.png}
\end{figure}

La segona comanda, \texttt{source}, el que fa és llegir el contingut del script que se li passa com a argument i executar el seu contingut en la \textit{shell} actual. Llavors, les variables que s'hagin modificat (entre altres) dins del script s'hauran modificat realment a la \textit{shell} actual.

\begin{figure}[ht]
    \centering
    \includegraphics[trim={0 2cm 0 0}, clip, width = \linewidth]{img/source.png}
\end{figure}

Per a fer aquests comportaments ``permanents'' s'han d'escriure en els fitxers d'inicialització. Cada \textit{shell} té els seus arxius i estan definits a la seva pàgina del manual. En el cas de \texttt{bash} alguns dels arxius són \texttt{$\sim$/.bashrc}, que s'executa en una shell que s'obre de forma interactiva (un emulador de terminal, per exemple) o \texttt{$\sim$/.profile}, que s'executa quan es fa login.

En aquests documents s'haurien d'escriure els alias que es volen tenir, com també definicions de variables o programes a executar en background, o altre configuració pertinent de la \textit{shell}.

Un petit exemple, per afegir la carpeta \texttt{scripts} que tenim al nostre home al \texttt{PATH} s'hauria d'afegir la següent línia a l'arxiu \texttt{$\sim$/.bashrc}:

\begin{boxed}
\texttt{export PATH=\$PATH:$\sim$/scripts/}
\end{boxed}
