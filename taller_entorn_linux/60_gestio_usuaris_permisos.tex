\section{Gestió d'usuaris i permisos}
\subsection{Gestió usuaris}
Qualsevol entorn GNU/Linux disposa d'un sistema d'usuaris tipus UNIX on s'associa un \textit{username} a un UID (\textit{user identifier}), que permet al sistema operatiu identificar un usuari de sistema i tota la informació que té relacionada. Els usuaris al cap i a la fi ens permeten diferenciar les dades i processos de cada usuari i fer així que un mateix equip el pugui fer servir molts usuaris diferents.

Existeixen diferents fitxers al sistema que contenen la informació necessària per identificar els usuaris, així com les característiques de cadascun, el grup al qual pertanyen i els permisos que tenen (més endavant parlarem sobre permisos).

El primer fitxer que ens interessa és el \texttt{/etc/passwd}:
\begin{figure}[H]
\centering
\includegraphics[width = \linewidth]{img/etc_passwd.png}
\label{fig:/etc/passwd}
\end{figure}
Aquest és l'aspecte que presenta el fitxer \texttt{/etc/passwd} i podríem dir que es tracta de la base de dades dels usuaris al sistema.\\
Cada línia té la següent estructura:

\begin{boxed}
      \texttt{username:passwd:uid:real\_name:homedir:shell}
\end{boxed}

\begin{itemize}
\item \texttt{username} = nom de l'usuari
\item \texttt{passwd} = camp especial de \textit{password} que ens indica amb una x que es troba encriptat
\item \texttt{uid} = identificador d'usuari
\item \texttt{real\_name} = nom complet d'usuari
\item \texttt{homedir} = directori del \texttt{home} d'usuari
\item \texttt{shell} = intèrpret de comandes de l'usuari
\end{itemize}
Si bé aquest fitxer és visible per tots els usuaris només l'usuari de \texttt{root} és capaç de modificar-lo.

\begin{boxed}
      \textbf{Exercici 1.} Quina diferencia hi ha entre \texttt{adduser} i \texttt{useradd}? Quina és la millor opció? Per  què?
\end{boxed}
\begin{boxed}
      \textbf{Exercici2.} Creeu un nou usuari amb el nom de \texttt{linus} i que tingui el seu directori de \texttt{home} a \texttt{/home/LinusTorvalds} (sempre es útil mirar el manual de la comanda \texttt{adduser}/\texttt{useradd}).
\end{boxed}

Un altre fitxer interessant relacionat amb els usuaris és el fitxer \texttt{/etc/shadow} que conté les contrasenyes dels usuaris del sistema xifrades. Només el superusuari pot accedir a aquest fitxer. De fet, quan utilitzem la comanda \texttt{passwd} per canviar la contrasenya d'un usuari, primer es comprova si l'usuari que ho demana té els permisos adequats i després s'executa l'escriptura al fitxer com a superusuari.

\begin{figure}[H]
\centering
\includegraphics[width = \linewidth]{img/etc_shadow.png}
\label{fig:/etc/passwd}
\end{figure}
Cada línia té la següent estructura:
\begin{boxed}
      \texttt{username:password:lastchanged:minimum:maximum:warn:inactive:expire}
\end{boxed}

\begin{itemize}
\item \texttt{username} = nom d'usuari
\item \texttt{password} = contrasenya encriptada
\item \texttt{lastchanged} = dies des de que es va canviar la contrasenya (per motius històrics al sistema UNIX es comença a contar a partir de l'1 de gener de 1970)
\item \texttt{minimum} = dies requerits entre canvis de contrasenyes
\item \texttt{maximum} = nombre de dies mentre una contrasenya és vàlida
\item \texttt{warn} = nombre de dies en que s'avisa que una contrasenya deixarà de ser vàlida
\item \texttt{inactive} = nombre de dies on el compte d'usuari es deshabilita temporalment després d'expirar la contrasenya
\item \texttt{expire} = nombre de dies en que el compte d'usuari es deshabilita completament i no es pot fer login mai més
\end{itemize}

La comanda \texttt{chage} ens permet modificar els valors dels paràmetres esmentats.
\begin{boxed}
      \textbf{Exercici 3.} Com modificaríeu els dies de \textit{warning} abans que una contrasenya expiri?
\end{boxed}

Totes aquestes opcions ens donen una certa flexibilitat a l'hora d'administrar usuaris i el comportament de les seves contrasenyes, donant més o menys per poder canviar-les o afegint una capa de seguretat desactivant comptes d'usuaris després d'un temps establert de forma automàtica.
 %TODO exercicis canviar paràmetres contrasenya
Finalment trobem un altra abstracció relacionada amb la gestió d'usuaris que és molt útil a l'hora d'assignar diferents permisos quan el nombre d'usuaris comença a ser elevat. Aquesta ens permet agrupar usuaris que tenen comportaments similars, poden accedir als mateixos arxius i executar certes comandes. Si bé podríem assignar aquests permisos de forma individual a cada usuari, un grup ens permet assignar aquests permisos a un conjunt, la qual cosa ens facilita el control i la gestió de permisos en sistemes amb una gran quantitat d'usuaris. Com en les abstraccions anteriors, també existeix un fitxer associat dins el nostre sistema que fa aquesta pinta:
\begin{figure}[H]
\centering
\includegraphics[width = \linewidth]{img/etc_group.png}
\label{fig:/etc/group}
\end{figure}
Cada línia té la següent estructura:
\begin{boxed}
      \texttt{group\_name:password:groupid:grouplist}
\end{boxed}

\begin{itemize}
\item \texttt{group\_name} = nom del grup
\item \texttt{password} = no s'utilitza normalment, però dóna la possibilitat de posar contrasenya a grups privilegiats
\item \texttt{groupid} = id del grup. Es troba també a \texttt{/etc/passwd} (si us adoneu un group no deixa de ser com un usuari que conté un o més usuaris)
\item \texttt{grouplist} = llista de noms dels usuaris que pertanyen al grup separats per comes
\end{itemize}
\begin{boxed}
      \textbf{Exercici 4.} Creeu un nou grup anomenat \texttt{FSF}. A continuació creeu un nou usuari anomenat \texttt{stallman} amb \textit{common name} \texttt{Richard Stallman} i que pertanyi a aquest nou grup que hem creat. Pista: mireu el manual de \texttt{useradd}
\end{boxed}

\subsection{Permisos}
Com hem parlat en la secció anterior, l'existència dels usuaris i dels grups ve determinada per les accions que poden executar aquests usuaris o els arxius que pertanyen a cada usuari. En molts casos ens pot interessar el control dels usuaris a cert contingut o funcionalitats del sistema, per tal d'establir seguretat a l'estabilitat del sistema. Un usuari que no sigui malintencionat pot igualment fer malbé la integritat del sistema tant com algú que realment vol aconseguir-ho, és per això que existeixen els permisos.

Podríem definir un permís com una regla que autoritza o no a un usuari a realitzar una certa acció: executar un programa, modificar un fitxer, llegir el seu contingut, etc.

S'apliquen tres tipus de permisos als fitxers i directoris del sistema: lectura, escriptura i execució. Alhora, aquests permisos es posen a tres àrees: propietari, grup o altres. Els permisos d'un fitxer o directori vénen definits per la següent estructura:
\begin{figure}[H]
\centering
\includegraphics[width = \linewidth]{img/permisos_estructura.png}
\label{fig: estructura permisos}
\end{figure}
El primer bloc ens indica si el fitxer es tracta d'un fitxer regular (marcat amb un guió) o d'un directori (marcat amb una \texttt{d}).

El tres següents blocs, en ordre, representen els permisos que té el propietari del fitxer, el grup o altres (qualsevol usuari).

Els últims dos blocs ens mostren qui és el propietari del fitxer i a quin grup pertany aquest fitxer.

Amb aquest exemple podem veure més clarament com és l'estructura dels permisos que poden rebre els fitxers:
\begin{figure}[H]
\centering
\includegraphics[width = \linewidth]{img/permisos_ls.png}
\label{fig:estructura permisos}
\end{figure}

Algunes comandes útils que ens permeten modificar els permisos dels fitxers són:
\begin{itemize}
\item \texttt{chown} = permet modificar el propietari
\item \texttt{chgrp} = permet modificar el grup d'un fitxer
\item \texttt{chmod} = permet modificar els permisos d'un fitxer
\end{itemize}
Si bé les dues primeres comandes tenen una sintaxi senzilla, \texttt{chmod} presenta dos modes per assignar permisos.

Amb la comanda \texttt{chmod} podem modificar els permisos amb els operadors \texttt{+}, \texttt{-} i \texttt{=}. Podem utilitzar aquests operadors per modificar els permisos del propietari \texttt{= u}, group \texttt{= g}, altres \texttt{= o} i tots els anteriors \texttt{= a}. A continuació, exposarem uns exemples que ens permetran entendre millor com podem modificar permisos amb aquesta comanda.
\begin{figure}[H]
\centering
\includegraphics[width = \linewidth]{img/permisos_exemple_1.png}
\label{fig: permisos exemple 1}
\end{figure}
En aquest exemple tenim un fitxer anomenat \texttt{exemple\_1} que té els permisos per defecte quan es crea un fitxer nou.\\
A continuació volem donar permisos d'escriptura pel group i altres usuaris perquè qualsevol usuari pugui escriure sobre ell.\\
Finalment, decidim treure tant els permisos d'escriptura com lectura del grup i els altres usuaris per obtenir com a resultat un fitxer que només pot ser llegit i escrit per l'usuari \textit{owner}.

Una manera alternativa d'assignar permisos fent servir la comanda \texttt{chmod} és a través de la codificació binaria dels permisos.

Podem desglossar els permisos de cada bloc (\textit{owner}, \textit{group} i \textit{others}) en tres números. Aquests números ens permeten codificar els permisos de la següent manera:
\begin{itemize}
\item \texttt{0}: (000) Sense permisos
\item \texttt{1}: (001) Permís d'execució
\item \texttt{2}: (010) Permís d'escriptura
\item \texttt{3}: (011) Permisos d'escriptura i execució
\item \texttt{4}: (100) Permisos de lectura
\item \texttt{5}: (101) Permisos de lectura i execució
\item \texttt{6}: (110) Permisos de lectura i escriptura
\item \texttt{7}: (111) Tots els permisos
\end{itemize}
El següent exemple ens permetrà entendre-ho millor:
\begin{figure}[H]
\centering
\includegraphics[width = \linewidth]{img/permisos_exemples_2.png}
\label{fig: permisos exemple 1}
\end{figure}
A continuació, volem modificar els seus permisos per tal que el propietari del fitxer pugui llegir i escriure sobre ell, el grup al qual pertany pugui llegir el fitxer i cap altre usuari més pugui realitzar cap acció sobre ell. 

D'aquest exemple podem extreure que podem assignar permisos molt fàcilment si som conscients de la codificació necessària per complir els requisits de permisos.

Per últim, mencionarem tres permisos especials que s'assignen a directoris i fitxers executables que poden ser útils en certs casos.

El primer permís especial que explicarem és el bit \texttt{setuid}. Aquest bit s'assigna a fitxers executables i permet als usuaris que executin aquell arxiu adquirir els permisos del propietari del fitxer. Podem assignar el bit amb la comanda:
\begin{boxed}
      \texttt{\$ chmod u+s fitxer}
\end{boxed}
Seguidament trobem el permís especial \texttt{setgid}. Trobem una analogia clara amb el permís \texttt{setuid}, donat que ens permet assignar els permisos del grup als fitxers que es troben dins d'un directori amb aquest permís especial. Podem assignar el bit amb la comanda:
\begin{boxed}
      \texttt{\$ chmod g+s directori}
\end{boxed}
Per últim però no menys important, presentem el permís \texttt{sticky} que ens pot resultar molt útil en directoris compartits on volem permetre que tothom pugui visualitzar els fitxers, però no modificar-los. Aquest permís ens permet que tot els fitxers que es troben dins del directori puguin ser llegits per tots els usuaris, però només els usuaris poden modificar els arxius dels que són propietaris. Podem assignar el bit amb la comanda:
\begin{boxed}
      \texttt{\$ chmod o+t directori}
\end{boxed}
