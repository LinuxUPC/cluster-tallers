\section{Instal·lació GNU/Linux}
En aquest apartat us mostrarem el procediment habitual d'instal·lació de la majoria de distribucions GNU/Linux de forma detallada i especificant en cada pas què és el que s'està fent i per què. 
El primer pas en la instal·lació és escollir la distribució GNU/Linux que volem instal·lar, en aquest sistema operatiu la decisió depèn de molts factors, ja sigui el manegador de paquets que fa servir, l'escriptori per defecte que incorpora o la filosofia pròpia de cada distribució. 
Existeixen distribucions per a la majoria de públics i per tant et recomanem que escollis la que més t'agradi o tingui una filosofia més propera a la teva idea.
\subsection{Consideracions inicials}
Alguns dels entorns gràfics més coneguts són: KDE Plasma (o només Plasma), Gnome, LXQT, Cinnamon, Mate, entre d'altres \cite{desktop_environment}.

\subsection{Distribucions GNU/Linux}
En aquest taller realitzarem la instal·lació de la distribució Arch Linux en el nostre sistema. Es tracta d'una de les distribucions més conegudes actualment, que disposa d'una \textit{wiki}\cite{installation_Arch} amb tota mena de documentació i una gran comunitat al darrere que s'encarrega de mantenir-la. 
Tot i que molts dels passos que realitzarem durant la instal·lació no són sempre necessaris, en aquest cas seguirem la guia d'instal·lació oficial de la seva \textit{wiki} que ens permetrà tenir un sistema funcional i que satisfarà les necessitats de la gran majoria d'usuaris. 
L'elecció d'aquesta distribució ve determinada per diversos factors.
El primer de tots és que el procés d'instal·lació es realitza íntegrament a través d'un terminal i es deixa en mans de l'usuari la configuració total del sistema, la qual cosa ens permetrà mostrar tots els passos necessaris per a la instal·lació d'aquesta.
Per altra banda, en tractar-se d'una distribució àmpliament coneguda i amb una comunitat molt gran al darrere, ens dóna la seguretat que en cas de tenir qualsevol problema podrem trobar una solució relativament fàcil cercant a la xarxa. 
Per últim i no menys important, acabar disposant d'una distribució GNU/Linux al nostre gust personal i entendre bé que es realitza en cadascun dels passos d'instal·lació.
Cal dir que la gran majoria de distribucions inclouen un instal·lador gràfic que accelera substancialment el temps d'instal·lació d'una distribució, però donen menys llibertat a l'hora de personalitzar el sistema que anem a instal·lar.
Tot i això, aquests instal·ladors acaben executant les mateixes passes que veurem a continuació, però fent servir la majoria de configuracions per defecte.
\subsection{Preparació del medi d'arrancada}
Un cop escollida la distribució cal descarregar la imatge corresponent des de la seva pàgina web, en el nostre cas \url{https://www.archlinux.org/download/}.
Un cop baixada la imatge, farem servir un dispositiu d'emmagatzematge com un CD/DVD o memòria USB (opció més recomanable) per gravar la imatge i que aquesta pugui carregar a l'engegar el nostre equip.
Per aquesta tasca recomanem les eines Rufus \url{https://rufus.ie/} i balenaEtcher \url{https://www.balena.io/etcher/} que es poden instal·lar en la majoria de sistemes operatius.
Quan tinguem aquesta imatge gravada, simplement hem de reiniciar el nostre equip i indicar des de la BIOS que volem arrencar des del nostre dispositiu d'emmagatzematge.
Si teniu dificultat en aquesta tasca us recomanem cercar per la xarxa com fer-ho, ja que es tracta d'una tasca molt senzilla i documentada que no us costarà gaire trobar.

Si hem accedit correctament a la imatge Arch Linux que es troba al nostre dispositiu d'emmagatzematge, hauríem de veure una imatge similar a aquesta:
\begin{figure}[H]
\centering
\includegraphics[width = \linewidth]{img/instalacio_1.JPG}
\label{fig:Command Line Installation}
\end{figure}

\subsection{Layout del teclat}
Donat que el \textit{layout} de teclat per defecte durant la instal·lació es tracta de l'anglès, haurem de canviar-lo (per una major facilitat d'ús) al \textit{layout} espanyol.
\begin{boxed}
      \texttt{\# loadkeys es}
\end{boxed}

Amb aquesta comanda estarem indicant que el \textit{layout} que farem servir serà l'"es" (spanish \textit{layout}).

\subsection{Mode de Boot}
El següent pas per a poder continuar és comprovar si el mode de boot del nostre sistema és BIOS/Legacy o UEFI.\cite{bios_vs_uefi}
\begin{boxed}
      \texttt{\# ls /sys/firmware/efi/efivars}
\end{boxed}

Si el directori no existeix vol dir que el mode de boot es tracta de BIOS/Legacy, en cas contrari es tractarà d'UEFI. Ara mateix només hem de tenir en compte quin és el mode i més endavant es detallaran els diferents processos d'instal·lació per a cadascun.

\subsection{Connexió a Internet}
Seguidament comprovarem si tenim accés a la xarxa. En cas que estiguem connectats per cable Ethernet no ens caldrà cap configuració addicional, per tant podrem comprovar la connexió fent un ping a la plana web d'Arch Linux.
\begin{boxed}
      \texttt{\# ping archlinux.org}
\end{boxed}

Si rebem resposta podrem continuar. En el cas que fem servir una xarxa Wi-Fi per accedir a Internet, ens caldrà configurar-la amb l'eina \texttt{wifi-menu}, la qual ens aporta una interfície gràfica, que ens permetrà connectar-nos a aquella xarxa.
\begin{boxed}
      \texttt{\# wifi-menu}
\end{boxed}

\subsection{Sistema de Fitxers}
Per encabir el nostre sistema primer hem de crear el sistema de fitxers corresponent que li donarà suport. Abans de configurar el sistema de fitxers ens cal crear les particions de disc necessàries per al nostre sistema de fitxers. Qualsevol instal·lació d'una distribució GNU/Linux necessita d'una partició per al directori \texttt{/} com a mínim, i una partició \texttt{efi} en cas que el sistema tingui un mode de boot UEFI.

Com hem mencionat abans, per una instal·lació mínima d'un sistema ens cal una única partició \texttt{/}. Addicionalment podem tenir una partició separada per al \texttt{/home}, on s'encabeixen les dades de cada usuari del sistema, i una partició de \texttt{swap}. 

Aquesta partició de \texttt{swap} és gairebé imprescindible en sistemes que disposen d'una memòria RAM reduïda. Això es deu a que permet al sistema copiar contingut de la RAM al disc quan aquesta està plena per poder donar espai a altres continguts. Per norma general, encara que no existeix consens en aquesta qüestió, la mida de la partició de \texttt{swap} ha de ser igual al doble de la quantitat de memòria RAM, tot i que en sistemes amb 8 GB de RAM o més és complicat que s'empleni per complet la memòria i per això no cal assignar un espai determinat. 

Per altra banda, la decisió de fer servir el \texttt{/home} per separat no aporta més o menys rendiment, però sí que ens permetria potencialment tenir els \texttt{/home} de cada usuari en un disc diferent del d'instal·lació o fins i tot poder reinstal·lar un sistema GNU/Linux conservant aquest directori. 

Un cop hem decidit en quantes particions volem tenir el nostre sistema hem de trobar els discos on volem instal·lar el sistema, una manera senzilla de llistar els dispositius d'emmagatzematge és amb la comanda:

\begin{boxed}
      \texttt{\# fdisk -l}
\end{boxed}

Que ens hauria de retornar una imatge semblant a aquesta:
\begin{figure}[H]
    \centering
    \includegraphics[width = \linewidth]{img/instalacio_2.JPG}
    \label{fig:fdisk -l}
\end{figure}

Podem observar que s'han llistat els discos del nostre sistema, podem identificar el nostre disc amb la mida que es mostra o amb el model que ens mostra. En aquest cas el nostre disc es tracta del \texttt{/dev/sda} amb una capacitat de 20 GB. Donat que es tracta d'una instal·lació fent servir una màquina virtual, no es llista el mitjà d'emmagatzematge on es troba la imatge del sistema (USB o CD/DVD), però també hauria d'apareixer, per tal d'evitar confusions amb aquest fet.

Per editar les particions del disc on volem instal·lar el sistema cal executar la comanda:
\begin{boxed}
      \texttt{\# fdisk /dev/nom\_disc}
\end{boxed}

\begin{figure}[H]
    \centering
    \includegraphics[width = \linewidth]{img/instalacio_3.JPG}
    \label{fig:fdisk /dev/nom_disc}
\end{figure}
\subsubsection{BIOS/Legacy}
Primer de tot hem de crear una taula de particions nova, és per això que premerem \texttt{o}, que crearà una nova taula de particions \texttt{MBR}, donat que el nostre sistema té el mode de boot BIOS/Legacy. En aquest cas particular crearem una partició \texttt{/} i una partició de \texttt{swap}, tot i que com hem esmentat abans no és sempre necessari. 

Per crear una nova partició premem \texttt{n}. Ens demanarà si volem que sigui del tipus primària o \textit{extended}, premem \texttt{p} per definir que es tracta d'una partició primària. Definim el número de partició, que pot ser de l'1 al 4. Encara que no té gaire influència la seva elecció, l'habitual és seguir un cert ordre, per això seleccionem 1. Premem \texttt{ENTER} per deixar el primer sector per defecte. A continuació ens demanarà l'últim sector, que ens definirà la mida de la partició. En aquest cas, com volem tenir 2 GB de partició de \texttt{swap},  la partició \texttt{/} serà de 18 GB. Per definir la mida premem \texttt{+ <número mida>}, que en aquest exemple es correspon amb \texttt{+18G}. Ens mostrarà per pantalla que s'ha creat una nova partició de 18 GB i de tipus \texttt{Linux}.

Ara ens cal crear la partició de \texttt{swap}, farem les mateixes comandes que per crear la partició \texttt{/}, però en aquest cas tant pel primer sector com per l'últim podem prémer \texttt{ENTER} per deixar-los per defecte. Com al paràgraf anterior, ens diu que ha creat una partició nova, en aquest exemple de 2 GB i de tipus \texttt{Linux}. Com volem que sigui la partició de \texttt{swap} cal que el seu tipus sigui \texttt{Linux swap}, podem llistar tots els tipus amb \texttt{l}. Per canviar el tipus de la partició de \texttt{swap} premem \texttt{t}, seleccionem la partició (en aquest cas la 2) i introduïm el codi \texttt{82} que es correspon amb \texttt{Linux swap}.

Per últim ens cal assenyalar que la partició \texttt{/} és on bootejara el sistema a l'arrencar l'ordinador. Per fer això premem \texttt{a} i el número de la partició (en aquest cas l'1). Podem prémer \texttt{p} per mostrar la llista de particions, que hauria de ser semblant a la següent imatge:
\begin{figure}[H]
    \centering
    \includegraphics[width = \linewidth]{img/instalacio_4.JPG}
    \label{fig:particions_bios}
\end{figure}
Arribat aquest punt podem guardar els canvis al disc i sortir del programa \texttt{fdisk} prement \texttt{w}. \\
A continuació, hem de formatar les particions que acabem de crear amb el sistema de fitxers corresponent. Per a la partició \texttt{/} recomanem fer servir el sistema de fitxers \texttt{ext4}, ja que és el més habitual, tot i que existeixen multitud d'alternatives.\cite{sistemes_fitxers}\\
Per formatar la partició amb el sistema de fitxers \texttt{ext4} executem la següent comanda:
\begin{boxed}
      \texttt{\# mkfs.ext4 /dev/sda1}
\end{boxed}

Seguidament hem de formatar la partició de \texttt{swap}, per això executem la comanda:
\begin{boxed}
      \texttt{\# mkswap /dev/sda2}
\end{boxed}

El següent pas és muntar les nostres particions al sistema de fitxers ja existent (el del memòria USB USB o CD/DVD) per a poder instal·lar allà el nostre sistema.

Per muntar la partició \texttt{/} al sistema de fitxers ens cal executar:
\begin{boxed}
      \texttt{\# mount /dev/sda1 /mnt}
\end{boxed}

En aquest cas muntem la partició en el directori \texttt{/mnt}, ja que es tracta d'un conveni establert, però no és obligatori i podríem muntar el nostre sistema en qualsevol directori lliure. 

Finalment hem d'activar la partició de \texttt{swap} amb la comanda:
\begin{boxed}
      \texttt{\# swapon /dev/sda2}
\end{boxed}


\subsubsection{UEFI}
Primer de tot hem de crear una taula de particions nova, és per això que premerem \texttt{g}, que crearà una nova taula de particions GPT donat que el nostre sistema té el mode de \textit{boot} UEFI. En aquest cas particular crearem una partició \texttt{/}, una partició \texttt{efi} i una partició de \texttt{swap} tot i que com hem esmentat abans no és sempre necessari. 

Per crear una nova partició premem \texttt{n} i definim el número de partició, que pot ser de l'1 al 128. Encara que no té gaire influència la seva elecció, l'habitual és seguir un cert ordre, per això seleccionem 1. Premem \texttt{ENTER} per deixar el primer sector per defecte. A continuació, ens demanarà l'últim sector, que ens definirà la mida de la partició, en aquest cas com volem tenir 512 MB de partició de \texttt{efi}. Per definir la mida premem \texttt{+ <número mida>}, que en aquest exemple es correspon amb \texttt{+512M}. Ens mostrarà per pantalla que s'ha creat una nova partició de 512 MB i de tipus \texttt{Linux}. En aquest cas la nostra partició \texttt{efi} ha de tenir un tipus diferent de \texttt{Linux}, per tant canviarem el seu tipus prement \texttt{t} i introduint el codi \texttt{1}, que es correspon amb el tipus \texttt{efi}.

Ara crearem una partició de \texttt{swap}, com hem creat les particions anteriors, prement \texttt{ENTER} per deixar els valors per defecte del primer sector i indicant que volem que sigui de 2 GB prement \texttt{+2G} quan ens demana l'últim sector. Com volem que sigui la partició de \texttt{swap} cal que el seu tipus sigui \texttt{Linux swap}, podem llistar tots els tipus amb \texttt{l}. Per canviar el tipus de la partició de \texttt{swap} premem \texttt{t}, seleccionem la partició (en aquest cas la 2) i introduïm el codi \texttt{19} que es correspon amb \texttt{Linux swap}.

Per últim ens cal crear la partició \texttt{/}, com hem creat les particions anteriors prement \texttt{ENTER} per deixar els valors per defecte. Com a l'apartat anterior ens diu que ha creat una partició nova, en aquest cas de 17.5 GB i de tipus \texttt{Linux}.

Per últim ens cal assenyalar que la partició \texttt{efi} és on bootejarà el sistema a l'arrencar l'ordinador, per fer això premem \texttt{a} i el número de la partició (en aquest cas l'1).
Podem prémer \texttt{p} per mostrar la llista de particions, que hauria de ser semblant a la següent imatge:
\begin{figure}[H]
    \centering
    \includegraphics[width = \linewidth]{img/instalacio_5.JPG}
    \label{fig:particions_uefi}
\end{figure}
Arribat aquest punt podem guardar els canvis al disc i sortir del programa \texttt{fdisk} prement \texttt{w}. 

A continuació hem de formatar les particions que acabem de crear amb el sistema de fitxers corresponent. Per a la partició \texttt{/} recomanem fer servir el sistema de fitxers \texttt{ext4}, que és el més habitual tot i que existeixen multitud d'alternatives.

Per formatar la partició amb el sistema de fitxers \texttt{ext4} executem la següent comanda:

\begin{boxed}
      \texttt{\# mkfs.ext4 /dev/sda3}
\end{boxed}

Seguidament hem de formatar la partició de \texttt{swap}, per això executem la comanda:
\begin{boxed}
      \texttt{\# mkswap /dev/sda2}
\end{boxed}

Per últim formatarem la partició \texttt{efi} amb la comanda:
\begin{boxed}
      \texttt{\# mkfs.fat -F32 /dev/sda1}
\end{boxed}

El següent pas és muntar les nostres particions al sistema de fitxers ja existent (el del memòria USB USB o CD/DVD) per a poder instal·lar allà el nostre sistema.

Per muntar la partició \texttt{/} al sistema de fitxers ens cal executar:
\begin{boxed}
      \texttt{\# mount /dev/sda3 /mnt}
\end{boxed}

En aquest cas muntem la partició en el directori \texttt{/mnt}, ja que es tracta d'un conveni establert, però no és obligatori i podríem muntar el nostre sistema en qualsevol directori lliure. 

A continuació muntarem la partició \texttt{efi} al directori \texttt{/mnt/efi} amb la comanda:
\begin{boxed}
      \texttt{\# mount /dev/sda1 /mnt/efi}
\end{boxed}

En cas que no existeixi el directori el crearem i activarem la partició \texttt{swap} amb les comandes: 
\begin{boxed}
      \texttt{\# mkdir /mnt/efi} \\
      \texttt{\# swapon /dev/sda2}
\end{boxed}

Amb això ja hem creat el nostre sistema de fitxers i l'hem muntat correctament per a poder realitzar la instal·lació del nostre sistema sobre ell.

\subsection{Instal·lació del sistema i generació fitxer \texttt{fstab}}
Un cop preparat el nostre sistema de fitxers ja podem instal·lar el nostre sistema, per això executem la següent comanda (sempre assegurant-nos que tenim accés a Internet):
\begin{boxed}
      \texttt{\# pacstrap /mnt base linux linux-firmware}
\end{boxed}

A continuació, generarem el fitxer \texttt{fstab} que s'encarrega de llistar els discos i les particions, i establir les relacions entre aquests perquè la màquina sàpiga els punts de muntatge de cada partició a l'arrancar el sistema. Per generar el fitxer \texttt{fstab} executem la comanda:
\begin{boxed}
      \texttt{\# genfstab -U /mnt >> /mnt/etc/fstab}
\end{boxed}

Per últim accedirem al sistema que acabem d'instal·lar.
\begin{boxed}
      \texttt{\# arch-chroot /mnt}
\end{boxed}

Ara mateix ens trobem dins de la nostra instal·lació. Ens caldrà realitzar les configuracions pertinents perquè el pròxim cop que arrenqui la màquina, aquesta accedeixi a la nova instal·lació i no al mitjà d'emmagatzematge que hem fet servir per la instal·lació.

\subsection{Configuració del sistema}
En aquest apartat realitzarem la configuració bàsica per tal que el nostre sistema pugui arrencar correctament en reiniciar la màquina.
\subsubsection{Boot loader}
Per aconseguir això primer ens cal instal·lar el software que s'encarrega de trobar en temps d'arrencada el sistema operatiu que es vulgui arrencar. És aquí on entra el \textit{boot loader}.\\
Existeixen multitud de \textit{boot loaders} diferents, però en aquest cas instal·larem el \textit{boot loader} \texttt{GRUB}.\cite{grub} \\
Per instal·lar el paquet \texttt{GRUB} executarem la comanda:
\begin{boxed}
      \texttt{\# pacman -S grub}
\end{boxed}

Per instal·lar \texttt{GRUB} en el nostre sistema executarem en funció del mode de \textit{boot}:
\begin{itemize}
\item BIOS/Legacy:
\begin{boxed}
      \texttt{\# grub-install --target=i386-pc /dev/sda}
\end{boxed}

\item UEFI:
\begin{boxed}
      \texttt{\# grub-install --target=x86\_64-efi --efi-directory=directori /efi --bootloader-id=GRUB}
\end{boxed}

\end{itemize}
Per últim generarem el fitxer de configuració del \texttt{GRUB} amb la comanda:
\begin{boxed}
      \texttt{\# grub-mkconfig -o /boot/grub/grub.cfg}
\end{boxed}


\subsubsection{Gestor de xarxa}
Tot i que podem activar manualment la xarxa cada cop que iniciem la màquina, per comoditat ens interessa que el sistema en temps de \texttt{boot} activi la xarxa perquè la puguem fer servir només arrencar. En aquests cas farem servir un software anomenat \texttt{NetworkManager} que s'encarregarà d'aquesta tasca.

Per instal·lar \texttt{NetworkManager} ens cal executar la comanda:
\begin{boxed}
      \texttt{\# pacman -S networkmanager}
\end{boxed}

Un cop finalitzada la instal·lació, volem que aquest software s'executi en temps d'arrencada. Per això necessitem activar el servei (més endavant es detallarà què és) associat a aquest software. Per això executem:
\begin{boxed}
      \texttt{\# systemctl enable NetworkManager}
\end{boxed}

\subsubsection{Últims passos}
Donat que l'usuari que hem fet servir durant tot aquests passos és l'usuari \texttt{root} (privilegiat), ens cal protegir-lo d'alguna manera mitjançant la configuració d'una contrasenya. 

Per això ens cal executar la comanda: 
\begin{boxed}
      \texttt{\# passwd}
\end{boxed}

Seguidament ens interessarà crear un nou usuari i configurar l'accés a \texttt{sudo}. Més endavant es detallarà que comporta això, però en resum, ens permet executar accions privilegiades (que només pot executar l'usuari \texttt{root}) fent servir un usuari qualsevol al que li hem donat permís prèviament. També podríem fer servir tota l'estona l'usuari root, però això suposa una mala pràctica de seguretat i en alguns casos realitzar accions que puguin posar en perill la integritat del sistema. 

Per crear un nou usuari executarem la següent comanda:
\begin{boxed}
      \texttt{\# useradd -m -g users -G wheel nomusuari}
\end{boxed}

Seguidament configurarem una contrasenya per al nou usuari:
\begin{boxed}
      \texttt{\# passwd nomusuari}
\end{boxed}

Per donar permisos de \texttt{sudo} al nou usuari primer ens cal instal·lar el software \texttt{sudo} amb la següent comanda:
\begin{boxed}
      \texttt{\# pacman -S sudo}
\end{boxed}

Seguidament hem d'incloure el nostre usuari a la llista de \texttt{sudoers} (usuaris que tenen permís \texttt{sudo}), per fer això executem la següent comanda:
\begin{boxed}
      \texttt{\# visudo}
\end{boxed}

Aquesta comanda ens permet editar el fitxer de \texttt{sudoers} i fa servir l'editor de text \texttt{vi} (inclòs en qualsevol entorn GNU/Linux). Per escriure en el document premem la tecla \texttt{i} per passar al mode inserció. A continuació hem d'afegir la següent línia al fitxer:
\begin{boxed}
      \texttt{\# nomusuari ALL=(ALL)}
\end{boxed}

Per guardar els canvis, primer hem de prémer \texttt{ESC} i després prémer \texttt{:wq}.

A partir d'aquest moment, quan iniciem sessió amb el nostre usuari podrem executar comandes privilegiades (que només executa l'usuari \texttt{root}) afegint \texttt{sudo} davant la comanda que vulguem executar.

\subsection{Finalització de la instal·lació}
Ara mateix disposem d'una instal·lació bàsica d'un entorn GNU/Linux que ens permetrà fer les tasques demanades a la resta del taller.

Com a tasques que es poden fer a continuació podem incloure la instal·lació d'un entorn gràfic i la resta del software que podem requerir.
