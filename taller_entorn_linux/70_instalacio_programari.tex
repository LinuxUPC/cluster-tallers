\section{Instal·lació de programari}

Una de les parts més importants d'un sistema són les aplicacions de les que aquest disposa. Generalment, un sistema serveix de poc si no té software amb el que es pugui interactuar.


Seguint la jerarquia de directoris del sistema vist a l'apartat \ref{sec:jerarquia_directoris}, hi ha diferents llocs on instal·lar programari.
El primer punt d'instal·lació (o prefix) és \texttt{/}, és a dir, \texttt{/bin} per als executables, \texttt{/lib} per a les llibreries, etc. En aquest directori no s'hauria d'instal·lar res de nou.
El segon punt és \texttt{/usr}. Aquest directori és el que utilitzen per defecte els gestors de paquets per a instal·lar les seves aplicacions.
Un altre punt és \texttt{/usr/local}, que s'utilitza per l'instal·lació d'aplicacions de forma manual.
L'últim punt d'instal·lació és \texttt{/opt}. S'utilitza generalment per a instal·lar aplicacions propietàries en el seu propi directori dins de \texttt{/opt}, ja que aquestes no acostumen a seguir l'estructura de les aplicacions convencionals. 

Una altra forma de fer una instal·lació seria fer-la en un directori a part i un cop feta, si es necessita, afegir un \textit{softlink} de l'aplicació a un dels directoris anteriors. Aquest mètode permet mantenir diferents versions instal·lades i també facilita la seva desinstal·lació.

Aquests directoris serveixen per mantenir ordenat el sistema, però realment una aplicació pot estar "instal·lada" a qualsevol part del sistema. Per executar-la només s'ha d'escriure la seva localització a la consola, o afegir aquesta localització a la variable d'entorn \texttt{PATH}.

En el cas que es vulgui instal·lar alguna aplicació per a només per a un usuari en concret, aquesta es pot instal·lar al \texttt{home} d'aquest usuari.

\subsection{Procés d'instal·lació general}
Per a instal·lar software en un sistema GNU/Linux (o a qualsevol UNIX en general) s'han de seguir uns certs passos si es vol mantenir el sistema net i mantenir l'estructura que porta:
\begin{enumerate}
    \item Seleccionar un punt d'instal·lació.
    \item Crear directoris d'instal·lació. \\
    És a dir, crear els següents directoris:
    \begin{itemize}
        \item[--] \texttt{\$PUNT/bin},
        \item[--] \texttt{\$PUNT/lib},
        \item[--] \texttt{\$PUNT/share},
        \item[--] \texttt{\$PUNT/share/doc} per a altres arxius i la documentació i,
        \item[--] \texttt{/etc} per als arxius de configuració.
    \end{itemize}
    \item Distribuir els fitxers propis de l'aplicació en els directoris que convingui.
    \item Crear una configuració inicial de l'aplicació.
\end{enumerate}

Per sort, aquest procés està automatitzat per a la majoria de distribucions i per a la majoria de software a través de sistemes de gestió de paquets, com poden ser: \texttt{apt} per a distribucions basades en Debian o \texttt{pacman} per a Arch Linux. A aquests gestors se'ls demana que instal·lin una aplicació i elles mateixes realitzen tot el procés anterior, fins i tot proporcionar una configuració per defecte.


En el cas que es vulgui una configuració diferent a la per defecte de la distribució, aquesta s'ha d'afegir en el \texttt{home} de l'usuari que vol aquesta configuració o canviar-la a \texttt{/etc} si és a nivell de sistema. Si es volcustomitzar encara més l'aplicació o el nostre gestor de paquets no té l'aplicació que necessitem, s'haurà de baixar el codi font i compilar-lo amb les modificacions que es vulguin.

\subsection{Formes d'instal·lar aplicacions}
Hi ha 3 maneres d'instal·lar aplicacions:
\begin{itemize}
    \item A partir de codi font.
    \item Utilitzant binaris pre-compilats.
    \item Utilitzant binaris autoinstal·lables.
\end{itemize}

%\subsubsection*{A partir de codi font}
\subsubsection{A partir de codi font}
Instal·lar programari a partir de codi font significa haver de compilar aquest codi. La majoria de cops no suposarà res complicat, ja que es pot reduir a una comanda simple de \texttt{make}. 

Per tant, per instal·lar software des de codi font s'han de seguir els passos següents:
\begin{enumerate}
    \item \textbf{Baixar-se} el codi (des de GitHub, SourceForge, etc.) i \textbf{descomprimir-lo}.
    \item \textbf{Llegir} la documentació (\texttt{README}, \texttt{INSTALL}, etc.). IMPORTANT!
    \begin{itemize}
        \item Pot contenir informació important de què i com fer la instal·lació.
    \end{itemize}
    \item \textbf{Instal·lar} dependències.
    \begin{itemize}
        \item Utilitzant qualsevol dels 3 mètodes.
    \end{itemize}
    \item \textbf{Identificar} quin sistema de compilació utilitza. \\
    Això se sap pel \texttt{README}/\texttt{INSTALL} o mirant els arxius que s'han descomprimit.
    \begin{itemize}
    % TODO: revisar format de les comandes, passar a quadradito?
        \item Si utilitza \textbf{autotools}: \\
        S'haurà d'executar la comanda \texttt{./configure --prefix=} i tot seguit el directori on es vulgui instal·lar.\\
        La comanda \texttt{./configure --help} ensenya totes les opcions disponibles.
        \item Si utilitza \textbf{cmake}: \\
        S'haurà d'executar la comanda \texttt{cmake -DCMAKE\_INSTALL\_PREFIX=} i tot seguit el directori on es vulgui instal·lar.
    \end{itemize}
    Aquestes comandes serveixen per crear el \texttt{Makefile} que després s'utilitzarà per compilar, les quals són molt més complexes del que s'ha explicat.
    % referencia curs compilació (?)
    \item \textbf{Compilar} fent  \texttt{make}.
    \item \textbf{Instal·lar} amb \texttt{make install}.
    \begin{itemize}
        \item Segurament es necessitin permisos de superusuari per a executar aquesta comanda.
    \end{itemize}
    \item Crear una \textbf{configuració} adequada per al nostre sistema.
\end{enumerate}

%\subsubsection*{Utilitzant binaris autoinstal·lables}
\subsubsection{Utilitzant binaris autoinstal·lables}
La pròpia empresa que ha desenvolupat l'aplicació crea un instal·lador per a ella. Seguint les instruccions d'aquest instal·lador s'aconseguirà instal·lar l'aplicació. 

S'ha de tenir en compte que aquest sistema d'instal·lació, al estar preparat per l'empresa desenvolupadora, prepara i configura els paquets pensant en l'aplicació i no en el sistema on ha d'executar-se. Això pot provocar algunes incompatibilitats. 

A més, aquest programari no acostuma a ser modificable.
Alguns exemples són MATLAB o la majoria d'aplicacions no obertes.


%\subsubsection*{Utilitzant binaris pre-compilats}
\subsubsection{Utilitzant binaris pre-compilats}
Aquestes aplicacions s'instal·len a través del gestor de paquets del sistema. Aquests paquets són generats per la pròpia distribució i, per tant, s'adapten al sistema. Tot i això, al ser pre-compilats són molt poc modificables.

             % TODO: revisar format comandes, caixetes?
Per exemple, utilitzant \texttt{apt}, per saber el nom del paquet a la nostra distribució de l'aplicació que es vol instal·lar, es fa \texttt{apt-cache search} seguit de les paraules clau que defineixen l'aplicació. Un cop se sap el nom del paquet es fa \texttt{apt-get install} seguit del nom del paquet per instal·lar-la.


\subsection{Desinstal·lació d'aplicacions}
La desinstal·lació de programari depèn completament de la forma amb la que es van instal·lar.

Si l'aplicació ha estat instal·lada a través d'un gestor de paquets, en cap cas s'ha d'esborrar manualment, ja que aquest gestor manté un registre de les aplicacions que ha instal·lat al sistema. Aquest mateix gestor pot desinstal·lar aquesta aplicació.

Altrament, si ha estat instal·lada a partir de codi font i el \texttt{makefile} té una regla de desinstal·lació es pot desinstal·lar d'aquesta forma. Normalment la regla és \texttt{uninstall}, no confondre-la amb la regla \texttt{clean}. Si no en té, simplement s'haurà d'esborrar el directori on ha estat instal·lada.


\subsection{Exemples}
Per a posar en posar en pràctica aquest apartat instal·larem la mateixa aplicació, git, de varies formes: primer mitjançant el gestor de paquets d'Arch Linux, i després a través de codi font.\\

Per instal·lar una aplicació utilitzant un gestor de paquets el que s'ha de fer primer és refrescar el seu registre de paquets disponibles, si no s'ha fet recentment.\\
\begin{figure}[ht]
    \centering
    \includegraphics[trim={0 1cm 0 0}, clip, width = \linewidth]{img/update.png}
\end{figure}

Un cop fet, s'ha de saber el nom del paquet per a la nostra distribució, no acostumen a canviar gaire però s'ha d'estar segur. En el meu cas es pot veure que ja és instal·lat, però el procés no canvia.\\
\begin{figure}[ht]
    \centering
    \includegraphics[trim={0 0cm 0 0}, clip, width = \linewidth]{img/search.png}
\end{figure}

Veiem que el paquet es diu simplement \texttt{git}. En aquest cas no feia falta fer la cerca, ja que ja coneixíem el nom, però en altres casos pot ser útil. Ja el podem instal·lar.
\begin{figure}[ht]
    \centering
    \includegraphics[trim={0 0.5cm 0 0}, clip, width = \linewidth]{img/install.png}
\end{figure}

En el cas de apt, el gestor de paquets de les distribucions basades en Debian o Ubuntu, es fa de la següent forma:
\begin{boxed}
\texttt{\$ sudo apt update}\\
\texttt{\$ apt search git}\\
\texttt{\$ sudo apt install git}
\end{boxed}

\clearpage
Ara instal·larem el mateix programa però a través del codi font.
A la pàgina oficial de git veiem que té un enllaç al codi font, entrem i escollim la versió que ens interessa. Nosaltres escollirem la \texttt{2.20.1}.\\

\begin{figure}[ht]
    \centering
    \includegraphics[trim={0 5cm 0 0}, clip, width = \linewidth]{img/gitsrc.png}
\end{figure}

Descarreguem el codi font i el descomprimim.
\begin{figure}[ht]
    \centering
    \includegraphics[trim={0 0cm 0 0}, clip, width = \linewidth]{img/curl.png}
\end{figure}

Inspeccionant l'arxiu \texttt{INSTALL} veiem que no utilitza cmake. I podem seguir les instruccions que ens mostra. En el nostre cas el volem instal·lar a \texttt{$\sim$/bin}.
\begin{boxed}
\texttt{\$ make configure}\\
\texttt{\$ ./configure --prefix=\$HOME/bin/git}
\end{boxed}

En aquest moment podria sortir algun error. Si fos el cas, s'hauria de resoldre i re-executar l'última comanda anterior.\\

Si no ha donat cap error ja podem continuar a la compilació del programa fent:
\begin{boxed}
\texttt{\$ make all}\\
\texttt{\$ make install}
\end{boxed}
I ja hauria de ser instal·lat a on li hem indicat. Per comprovar-ho provarem d'executar-lo i mirar la versió de la aplicació instal·lada mitjançant el gestor de paquets i la nova que haurien de ser diferents perquè així ho hem escollit.
\begin{figure}[ht]
    \centering
    \includegraphics[trim={0 0cm 0 0}, clip, width = \linewidth]{img/version.png}
\end{figure}