\section{Serveis i processos}
En aquest apartat parlarem dels processos a Linux i un tipus especial de processos anomenats serveis. Explicarem què són els processos, què representen, com identificar-los, monitorar-los i la relació que tenen amb la resta del sistema. Per altra banda, explicarem què són els serveis i per què és important controlar quins són els serveis actius del nostre sistema.

\subsection{Processos a GNU/Linux}

Segons el \textit{Linux Documentation Project}, un procés és la unitat bàsica d'execució de codi en el nostre sistema \cite{processes}. Quan executem qualsevol programa al nostre sistema, se li assignarà un procés que s'encarregarà de reservar i gestionar els recursos necessaris, i executar les instruccions del programa a la CPU. No entrarem més en detall sobre com s'implementen els processos, però cal dir que un procés es representa amb una \textit{data structure} que conté tota la informació necessària per mantenir i gestionar aquest flux d'execució: estat, links a altres processos, identificador, timers, accés al sistema de fitxers, accés a memòria, context...

A Linux el màxim nombre de processos que poden córrer sobre el nostre sistema ve determinat al vector que conté les seves structures i que per defecte té una mida de 512. Cada procés s'identifica amb un \texttt{PID} (\textit{Process Identifier}) que és únic en tot el sistema, fent així que puguem identificar un procés saben el seu \texttt{PID} en qualsevol moment. Els processos segueixen una jerarquia en forma d'arbre, igual que el sistema de fitxers. Això es deu a que a l'iniciar el sistema existeix un procés especial anomenat \texttt{init}, que té el \texttt{PID} igual a 1, del qual pengen la resta de processos. D'igual forma, si un procés vol executar un de nou copiarà la seva informació en el nou procés i penjarà d'aquest a la jerarquia. Aquesta organització permet que qualsevol procés pugui accedir a la informació d'un altre si té els permisos necessaris.

Podem saber els processos que corren al nostre sistema amb la comanda:
\begin{boxed}
      \texttt{\$ ps}
\end{boxed}

Si aneu al manual corresponent de la comanda podreu observar que tenim diverses maneres de veure els processos de sistema.
\begin{boxed}
      \textbf{Exercici 1.} Comproveu els processos del vostre sistema amb la vista clàssica i la vista en arbre. Existeixen més de 512 processos? Com és possible?
\end{boxed}

\begin{boxed}
      \textbf{Exercici 2.} Obre un navegador web (Firefox, Chromium, Brave). Com podries trobar fàcilment el \texttt{PID} del procés que executa el navegador? Pista: \texttt{grep} és una eina molt potent.
\end{boxed}


Els processos poden tenir diferents estats depenent del seu estat d'execució:
\begin{itemize}
    \item \texttt{Running} = el procés s'està executant a la CPU o està esperant a ser assignat a una CPU
    \item \texttt{Waiting} = el procés està esperant a un esdeveniment o a que el sistema li doni els recursos necessaris
    \item \texttt{Stopped} = el procés està parat 
    \item \texttt{Zombie} = el procés no es troba en cap dels estats anteriors, però tampoc s'ha eliminat del sistema la seva structure
\end{itemize}
D'aquest llistat podem extreure diverses conclusions:
\begin{enumerate}
    \item Els recursos d'un sistema són limitats i per tant necessitem regular quan donar-los als nostres processos
    \item Podem utilitzar esdeveniments per parar processos o fer que esperin
    \item És interessant tenir un control sobre els processos \texttt{zombie}, ja que ocupen recursos al sistema igualment
\end{enumerate}
Una eina bàsica en la monitorització de sistemes és la comanda:
\begin{boxed}
      \texttt{\$ top}
\end{boxed}

Aquest programa es pot trobar en qualsevol entorn, i al contrari de \texttt{ps}, es tracta de una comanda interactiva que ens permetrà interactuar amb la informació que mostra.
\begin{boxed}
      \textbf{Exercici 3.} Proveu a executar la comanda \texttt{top}. Intenteu fer-vos a la idea de què pot representar la informació que apareix.
\end{boxed}

La comanda \texttt{top} mostra moltíssima informació, per això l'anirem desglossant poc a poc.
\begin{figure}[H]
    \centering
    \includegraphics[width=0.4\textwidth]{img/top1.png}
    \label{fig:top1}
\end{figure}
Aquesta primera part del \texttt{top} ens indica l'hora actual, el temps que porta encès el sistema i els usuaris actius que hi ha al sistema.
\begin{figure}[H]
    \centering
    \includegraphics[width=0.75\textwidth]{img/top2.png}
    \label{fig:top2}
\end{figure}
Seguidament trobem les estadístiques de processos que corren sobre el sistema: el nombre total i aquest total desglossat en els diferents estats.
\begin{figure}[H]
    \centering
    \includegraphics[width=0.75\textwidth]{img/top3.png}
    \label{fig:top3}
\end{figure}
La tercera fila del \texttt{top} ens mostra les estadístiques referides a l'ús de la CPU al nostre sistema: \texttt{us} (temps a usuari), \texttt{sy} (temps a sistema), \texttt{ni} (temps a processos amb \textit{nice}), \texttt{id} (temps en \textit{idle}), \texttt{wa} (temps \textit{waiting}), \texttt{hi} (temps d'interrupcions hardware), \texttt{si} (temps d'interrupcions software), \texttt{st} (temps de una màquina virtual en cas que en fem servir).
\begin{figure}[H]
    \centering
    \includegraphics[width=0.75\textwidth]{img/top4.png}
    \label{fig:top4}
\end{figure}
Per últim, mostra estadístiques sobre la memòria física i la virtual. De les dues ens dona estadístiques sobre el total de memòria, la que està lliure, en ús i en buffer/cache.
\begin{figure}[H]
    \centering
    \includegraphics[width=0.75\textwidth]{img/top5.png}
    \label{fig:top5}
\end{figure}
Per últim ens mostra estadístiques sobre els processos que corren sobre el sistema: \texttt{pid}, l'usuari al que pertany, la prioritat, el número \textit{nice} (cerqueu més informació sobre això), \texttt{virt}/\texttt{res}/\texttt{shr} ens mostra informació relacionada amb la memòria, l'estat del proces, percentatge d'ús de cpu i memòria, el temps que porta executant-se i la comanada que ha originat el proces.
\begin{boxed}
      \textbf{Exercici 4.} Com podríeu filtrar la sortida del top perquè només mostri informació sobre processos d'un usuari?
\end{boxed}

Si bé aquestes eines ens serveixen només per monitorar, de vegades es necessari prendre decisions sobre l'execució de certs processos. Per exemple podríem acabar l'execució de un procés que està consumint molts recursos en un cert moment. Donat que som capaços d'identificar un procés amb el seu \texttt{pid} (fent servir comandes com \texttt{ps} o \texttt{top}) podem enviar missatges a un procés en concret perquè acabi la seva execució.

Aquests missatges s'anomenen \textit{signals}. Es podrien entendre també com esdeveniments que fem arribar a processos i que obliguen al procés a reaccionar a ells si els tenen habilitats.

Podem enviar signals a processos amb la comanda:
\begin{boxed}
      \texttt{\$ kill}
\end{boxed}

Per defecte envia un \textit{signal} \texttt{SIGTERM} al procés seleccionat, acabant la seva execució.
\begin{boxed}
      \textbf{Exercici 5.} Torneu a obrir el vostre navegador preferit i cerceu el seu \texttt{PID} fent servir la comanda \texttt{ps} com hem fet anteriorment. Envia un \textit{signal} \texttt{SIGTERM} fent servir la comanda \texttt{kill} al procés del navegador.
\end{boxed}


\subsection{Serveis}
Qualsevol sistema UNIX conté \textit{scripts} que li permet controlar els \textit{daemons} que corren a un sistema i que ofereixen determinats serveis. En aquesta frase existeixen moltes paraules desconegudes i les anirem definint per parts.

Un \textit{script} és un programa simple que s'encarrega de realitzar tasques senzilles i concretes.

Un \textit{daemon} és un procés de sistema que corre en \textit{background} (no interactuem amb ell) i que realitza una tasca en concret.


D'això podem deduir que un servei és un programa que corre en \textit{background} i que es pot gestionar amb uns \textit{scripts} que ens ofereix el sistema.

Alguns dels serveis més comuns són els referits a l'interface gràfica o el controlador de xarxa, donat que ens interessa que tot així ja funcioni un cop s'ha completat el boot.

És per això que existeix un programa present en la majoria de distribucions GNU/Linux que ens permet gestionar tots els serveis del sistema. Aquest programa s'anomena \texttt{systemd} i s'executa durant el temps de boot.

Per norma general, quan instal·lem un programa fent servir el gestor de paquets del sistema ell mateix s'encarrega d'engegar els \textit{daemons} necessaris perquè funcioni aquell programa, tot i que no necessàriament és sempre així.

Podem gestionar el programa \texttt{systemd} amb la comanda:
\begin{boxed}
      \texttt{\$ systemctl}
\end{boxed}

Aquest programa ens permet gestionar els \textit{daemons} que s'executen a l'inici de boot o durant l'execució del sistema.

Per comprendre millor el funcionament d'aquest programa instal·larem un programa al nostre sistema i desprès activarem el \textit{daemon} necessari per a que funcioni correctament.


Aquest programa és SSH (Secure Shell), que permet obrir un shell en un altre màquina a través de la xarxa. Podem instalar SSH a través del gestor de paquets en el paquet \texttt{openssh}, anomenat així en la majoria de distribucions.
\begin{boxed}
    \texttt{Debian/Ubuntu : apt install openssh}\\
      \texttt{Arch/Manjaro : pacman -S openssh}\\
      \texttt{RedHat/Fedora: dnf install openssh}
\end{boxed}

Si bé amb aquesta instal·lació ja podríem connectar-nos a una màquina que tingui un servidor SSH corrent, també ens pot interessar accedir a la nostra màquina fent servir aquest programa. Per fer això necessitem que el \textit{daemon} del servidor de SSH estigui corrent en la nostra màquina. Iniciarem el \textit{daemon} amb la comanda \texttt{systemctl}:
\begin{boxed}
      \texttt{\# systemctl start sshd}
\end{boxed}

Executant aquesta comanda aconseguim iniciar el \textit{daemon}, però potser ens interessa més que durant el boot ja s'iniciï i no haver d'executar aquesta comanda cada cop que iniciem el nostre sistema. Si volem que s'iniciï en temps de boot executarem la següent comanda:
\begin{boxed}
      \texttt{\# systemctl enable sshd}
\end{boxed}

En cas que vulguem evitar que un \textit{daemon} s'executi en temps de boot executarem la següent comanda:
\begin{boxed}
      \texttt{\# systemctl disable sshd}
\end{boxed}

L'elecció de quins \textit{daemons} volem que corrin al nostre programa és un element de seguretat molt important, donat que són programes que corren en \textit{background} i dels que no tenim un control visual necessàriament. 
