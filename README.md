# Cluster de Tallers
Repositori que conté els continguts dels tallers de LinuxUPC 

Descàrrega: https://gitlab.com/LinuxUPC/cluster-tallers/-/jobs/artifacts/master/browse?job=tallers_deploy
## Taller compilació
Apendràs a compilar tot tipus de programes en un sistema GNU/Linux, des del més bàsic dels programes fins al més complex!
Faràs servir eines que automatitzen el procés com GNU/Make, configure o cmake.

## Taller d'entorn de Linux
Apendràs a entendre l'entorn d'un sistema basat en GNU/Linux. Des de les comandes més bàsiques a la gestiò d'usuaris, grups,
redireccions i comandes útils. Estaràs més còmode que mai en un sistema GNU/Linux! :)

## Taller git
Apendràs a fer servir l'eina de control de versions git. Entendràs què és un repositori, com es conforma, com treballar
en grup en un codi.

## Taller LaTeX
Apendràs a fer servir el procesador de textos LaTeX. Fes treballs com mai abans, escriu documents més ràpid que mai i 
amb resultats impoluts.

## Taller shell
Apendràs a fer servir el shell com mai abans, fer scripts per automatizar tasques.

## Taller Vim
Aprendràs a fer servir l'editor de text Vim, un dels més populars. 
