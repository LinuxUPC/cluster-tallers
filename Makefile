
TALLER_DIRS=$(wildcard taller_*)
TALLERS=$(foreach taller,$(TALLER_DIRS), $(taller)/$(taller).pdf)

all: $(TALLERS)

%.pdf: 
	$(MAKE) -C `dirname $@`
	$(MAKE) clean

clean: $(TALLER_DIRS)
	for i in $^; do $(MAKE) -C $$i $@; done

ultraclean: $(TALLER_DIRS)
	for i in $^; do $(MAKE) -C $$i $@; done
